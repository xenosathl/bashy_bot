extern crate dotenv;
extern crate slack;
mod bot;
mod networking;

use dotenv::dotenv;


fn main() {
    let token_var = "SLACK_TOKEN";
    dotenv().ok();
    let token = dotenv::var(token_var)
        .map_err(|_| std::env::var_os(token_var)
            .expect(&format!("No {} value. Set it in environment or in .env file.", token_var) )
    ).unwrap();

    let mut bot = bot::BashBot{};
    loop {
        slack::RtmClient::login_and_run(&token, &mut bot)
            .map_err(|err| {
                match err {
                    //TODO: separate behaviour on different errors
                    slack::Error::Api(_) => std::process::exit(1),
                    _ => (),
                }
            })
        .expect("Dunno bro");
    }
}

