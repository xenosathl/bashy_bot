use slack;
use crate::networking::FortuneHandler;

pub struct BashBot;

struct ResponseEnvelope(String, String);

impl BashBot {
    fn wake_up(&self, val: &str) -> bool {
        val == "!bash"
    }

    fn handle_message(&self, envelope_type: slack::Message) -> Option<ResponseEnvelope> {
        match envelope_type {
            slack::Message::Standard(msg) => {
                if msg.channel.is_none() || msg.text.is_none() {
                    return None;
                };
                let channel = msg.channel.unwrap();
                let text = msg.text.unwrap();
                if !self.wake_up(&text) {
                    return None;
                }

                //TODO: handle parameters
                let fortune = FortuneHandler::new().fetch_fortune().unwrap();

                let reply = format!("{}\n{}", fortune.url, fortune.text);
                let resp = ResponseEnvelope(channel, reply);
                return Some(resp);
            },
            _ => None
        }
    }
}

impl slack::EventHandler for BashBot {
    fn on_event(&mut self, cli: &slack::RtmClient, event: slack::Event) {
        let resp = match event {
            slack::Event::Message(type_) => self.handle_message(*type_),
            _ => None,
        };
        match resp {
            Some(envelope) => {
                let ResponseEnvelope(channel_id, text) = envelope;
                cli.sender().send_message(&channel_id, &text).unwrap();
            },
            None => (),
        }
    }

    fn on_connect(&mut self, _cli: &slack::RtmClient) {
//        FortuneHandler::new().fetch_fortune();
        println!("On connect");
    }

    fn on_close(&mut self, _cli: &slack::RtmClient) {
        println!("On close");
    }
}

