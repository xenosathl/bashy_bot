extern crate reqwest;
extern crate select;

pub struct Responde {
    pub url: String,
    pub text: String,
}

pub struct FortuneHandler;

impl FortuneHandler {
    pub fn new() -> FortuneHandler {
        FortuneHandler { }
    }

    pub fn fetch_fortune(&self) -> Option<Responde> {
        let uri = "http://www.bash.org.pl/random/";
        let resp = reqwest::blocking::get(uri).unwrap();

        if ! resp.status().is_success() {
            return None
        }

        let url = resp.url().clone();
        println!("{}", url);
        let document = select::document::Document::from_read(resp).unwrap();

        let divs_with_fortune = document.find(select::predicate::Class("quote")).collect::<Vec<_>>();
        if divs_with_fortune.len() != 1 {
            return None
        }

        //TODO: consume vector
        let fortune_div = divs_with_fortune.iter().next().unwrap();

        let mut fortune_story = String::new();
        for child in fortune_div.children().into_iter() {
            if let Some(text) = child.as_text() {
                fortune_story.push_str(text.trim());
                fortune_story.push('\n');
            }
        }

        println!("{}", fortune_story);
        Some(Responde { url: url.to_string(), text: fortune_story })
    }
}

